class Main extends eui.UILayer {
    /**
     * 非热更新代码的版本号-微信的配置在version.js文件里面
     */
    public static nativeVersion = 0;
    /**
     * 这个变量留给脚本中调用的
     */
    public static app: Main = null;
    protected createChildren(): void {
        super.createChildren();
        Main.app = this;
        this.checkCode();
    }
    /**
     * 检查代码使用本地的还是热更新的
     */
    private checkCode() {
        var version = 0;//服务器发来的版本号
        if (Main.nativeVersion > version) {//如果本地版本比服务器高则用本地代码
            this.addChild(new Game());
        }
        else {
            this.queryCode();
        }
    }
    /**
     * 请求热更新代码
     */
    private queryCode() {
        if (egret.Capabilities.runtimeType != egret.RuntimeType.WXGAME) {//如果不是微信环境，比如调试时则请求resoutce/hotsript下面的代码
            this.getCode();
        }
        else {//如果是微信的环境
            this.getCode();//从服务器请求代码，这里自己实现
        }
    }
    //所有文件都走热更新
    // private tsList = [
    //     "UIUtils.ts",
    //     "Start.ts",
    //     "Switch.ts",
    //     "Utils.ts",
    //     "Game.ts"
    // ];

    //只更新某个方法
    // private tsList = [
    //     "UpdateFunction.ts"
    // ];

    //只更新某个类
    private tsList = [
        "UpdateClass.ts"
    ];
    private runTime: qs.Runtime = new qs.Runtime(window);
    private getCode(): void {
        var request: egret.HttpRequest = new egret.HttpRequest();
        request.open("resource/hotscript/src/" + this.tsList.shift());
        request.withCredentials = false;
        request.addEventListener(egret.Event.COMPLETE, () => {
            var code = request.response;
            this.runTime.regedit(code);//注册代码
            if (this.tsList.length == 0) {
                this.runScript();
            }
            else {
                this.getCode();
            }
        }, this);
        request.send();
    }
    private runScript(): void {
        //以下是只更新某个方法的例子
        // new this.runTime.g.UpdateFunction();//执行热更新中的UpdateFunction脚本的构造，构造里面会重新对本地代码中的方法进行替换
        // this.addChild(new Game());//利用本地代码创建游戏场景


        //以下是只更新某个类的例子
        this.addChild(new Game());//利用本地代码创建游戏场景，在创建之前，this.runTime.regedit这个操作之后，Game类会被热更新中的Game类替换

        //以下是全部代码都启用热更新的例子
        // new this.runTime.g.test.Start();//实例化test.Start这个脚本类，这个脚本类可以直接从全局中获取到

        //以下是更为性能测试的例子
        // qs.run(`
        //     class Start{
        //         public constructor() {
        //             var time = Date.now();
        //             var count = 0;
        //             var a = 3;
        //             var b = 3434;
        //             var c = 232323;
        //             for (var i = 0; i < 100000; i++) {
        //                 count = a * 4 + c * 444 + b / 3 + i;
        //             }
        //             console.log("qyscript耗时：" + (Date.now() - time) + "ms");
        //         }
        //     }
        // `, 'Start', window);
        // var time = Date.now();
        // var count = 0;
        // var a = 3;
        // var b = 3434;
        // var c = 232323;
        // for (var i = 0; i < 100000; i++) {
        //     count = a * 4 + c * 444 + b / 3 + i;
        // }
        // console.log("原生耗时：" + (Date.now() - time) + "ms");
    }
}
