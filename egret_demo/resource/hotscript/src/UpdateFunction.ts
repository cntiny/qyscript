class UpdateFunction {
    public constructor() {
        this.update_Game_initIndexUI();
    }
    /**
     * 更新Game类中的initIndexUI方法
     */
    private update_Game_initIndexUI() {
        Game.prototype.initIndexUI = function () {
            this.question = null;
            this.tiled = null;
            this.removeChildren();
            var bg = new eui.Rect();
            bg.percentWidth = bg.percentHeight = 100;
            bg.fillColor = 0x000000;
            this.addChild(bg);
            var gameName = new eui.Label();
            gameName.top = 150;
            gameName.horizontalCenter = 0;
            gameName.text = "过年猜字";
            gameName.size = 60;
            gameName.textColor = 0xffffff;
            gameName.bold = true;
            this.addChild(gameName);
            var startButton = UIUtils.getButton(0x1694a1, "开始游戏");
            startButton.horizontalCenter = startButton.verticalCenter = 0;
            this.addChild(startButton);
            startButton.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
                this.initGameUI();
            }, this);
            console.log("热更新方法成功");
        }
    }
}